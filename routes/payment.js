var express = require('express');
var router = express.Router();
var Datastore = require('nedb')
  , db = new Datastore();
var fetch = require('node-fetch');

router.post('/', function(req, res, next) {
  
fetch('http://localhost:3000/card/token/'+req.body.token)
.then(res => res.json())
.then(json => {
  const result = {
    'date': new Date(),
    'token': req.body.token,
    'cardNumber': json.cardNumber,
    'type':json.type,
    'amount': req.body.amount
  }; 
  db.insert(result);
  res.send(result);
});
});

router.get('/token/:token', function(req, res, next) {
  db.find({ token: req.params.token }, { date: 1, type: 1, cardNumber: 1, amount: 1, _id: 0}).sort({ date: -1 }).exec(function (err, doc) {
    res.send(doc);
  });
});

module.exports = router;
