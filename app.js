var express = require('express');
var path = require('path');
var cors = require('cors')
var logger = require('morgan');

var paymentRouter = require('./routes/payment');

var app = express();

app.use(cors())

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/payment', paymentRouter);

module.exports = app;
